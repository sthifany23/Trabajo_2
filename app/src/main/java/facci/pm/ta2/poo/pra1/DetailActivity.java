package facci.pm.ta2.poo.pra1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView textViewNombre, textViewPrecio, textViewDescription;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        String object = getIntent().getStringExtra("object_id");
        textViewNombre = (TextView)findViewById(R.id.textViewNombre);
        textViewPrecio = (TextView) findViewById(R.id.textViewPrecio);
        textViewDescription = (TextView)findViewById(R.id.textViewDescription);
        imageView = (ImageView)findViewById(R.id.thumbnail);


        //accede a las propiedades del
        //object de tipo String: name, price, description e image.
        DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("objeto1");
        //Recibe el parametro añadido en el Layout activity_detail.xml

        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    //Recibe a las propiedades del object de tipo String: name, price, description e imagen.
                    String Precio1 = (String) object.get("price")+("\u0024");
                    String Descripcion1 = (String) object.get("description");
                    String Nombre1 = (String) object.get("name");
                    Bitmap ImagenBitmap = (Bitmap) object.get("image");
                    //Se guardan los datos
                    textViewPrecio.setText(Precio1);
                    textViewDescription.setText(Descripcion1);
                    textViewNombre.setText(Nombre1);
                    imageView.setImageBitmap(ImagenBitmap);

                }else {
                    //error
                    // FIN - CODE6
                }
            }
        });
        // FIN - CODE6

    }

}
